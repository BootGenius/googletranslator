﻿using BootGenius.GoogleTranslator.Exceptions;

namespace BootGenius.GoogleTranslator.EventArgs
{
    public class GetTextPronunciationCompletedEventArgs : System.EventArgs
    {
        #region Properties

        public byte[] Result { get; private set; }

        public TranslateException Error { get; private set; }

        #endregion

        #region .ctors

        public GetTextPronunciationCompletedEventArgs(byte[] data)
        {
            Result = data;
        }

        public GetTextPronunciationCompletedEventArgs(TranslateException exception)
        {
            Error = exception;
        }

        #endregion
    }
}

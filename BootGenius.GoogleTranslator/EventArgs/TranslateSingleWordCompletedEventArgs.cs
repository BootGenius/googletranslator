﻿using BootGenius.GoogleTranslator.Exceptions;
using BootGenius.GoogleTranslator.Model;

namespace BootGenius.GoogleTranslator.EventArgs
{
    public class TranslateSingleWordCompletedEventArgs : System.EventArgs
    {
        #region Properties

        public TranslationRootElement Result { get; private set; }

        public TranslateException Error { get; private set; }

        #endregion

        #region .ctors

        public TranslateSingleWordCompletedEventArgs(TranslationRootElement rootElement)
        {
            Result = rootElement;
        }

        public TranslateSingleWordCompletedEventArgs(TranslateException exception)
        {
            Error = exception;
        }

        #endregion
    }
}
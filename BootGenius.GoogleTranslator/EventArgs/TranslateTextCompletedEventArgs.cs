﻿using BootGenius.GoogleTranslator.Exceptions;

namespace BootGenius.GoogleTranslator.EventArgs
{
    public class TranslateTextCompletedEventArgs : System.EventArgs
    {
        #region Properties

        public string Result { get; private set; }

        public TranslateException Error { get; private set; }

        #endregion

        #region .ctors

        public TranslateTextCompletedEventArgs(string translation)
        {
            Result = translation;
        }

        public TranslateTextCompletedEventArgs(TranslateException exception)
        {
            Error = exception;
        }

        #endregion
    }
}
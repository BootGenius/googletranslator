﻿namespace BootGenius.GoogleTranslator.Enums
{
    public enum TranslateError
    {
        HttpRequestError,
        JsonParserError,
        UnknownError
    }
}

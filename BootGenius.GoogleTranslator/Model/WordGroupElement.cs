﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BootGenius.GoogleTranslator.Model
{
    [DataContract]
    public class WordGroupElement
    {
        [DataMember(Name="pos")]
        public string PartOfSpeech { get; set; }

        [DataMember(Name="terms")]
        public List<string> Terms { get; set; }

        [DataMember(Name = "entry")]
        public List<WordEntryElement> Entries { get; set; }
    }
}

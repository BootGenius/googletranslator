﻿using System.Runtime.Serialization;

namespace BootGenius.GoogleTranslator.Model
{
    [DataContract]
    public class SentenceElement
    {
        [DataMember(Name = "trans")]
        public string TranslatedText { get; set; }

        [DataMember(Name = "orig")]
        public string OriginalText { get; set; }

        [DataMember(Name = "translit")]
        public string TranslatedTranslit { get; set; }

        [DataMember(Name = "src_translit")]
        public string OriginalTranslit { get; set; }
    }
}

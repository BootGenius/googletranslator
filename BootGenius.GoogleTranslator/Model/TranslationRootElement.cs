﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BootGenius.GoogleTranslator.Model
{
    [DataContract]
    public class TranslationRootElement
    {
        [DataMember(Name = "sentences")]
        public List<SentenceElement> Sentences { get; set; }

        [DataMember(Name = "dict")]
        public List<WordGroupElement> WordGroups { get; set; }

        [DataMember(Name = "src")]
        public string SourceLanguage { get; set; }

        [DataMember(Name = "server_time")]
        public int ServerTime { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BootGenius.GoogleTranslator.Model
{
    [DataContract]
    public class WordEntryElement
    {
        [DataMember(Name="word")]
        public string WordText { get; set; }

        [DataMember(Name = "reverse_translation")]
        public List<string> ReverseTranslation { get; set; }

        [DataMember(Name="score")]
        public float Score { get; set; }
    }
}


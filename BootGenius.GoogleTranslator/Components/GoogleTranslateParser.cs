﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

using BootGenius.GoogleTranslator.Enums;
using BootGenius.GoogleTranslator.Exceptions;
using BootGenius.GoogleTranslator.Model;

namespace BootGenius.GoogleTranslator.Components
{
    public class GoogleTranslateParser
    {
        internal TranslationRootElement Parse(string jsonString)
        {
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(jsonString)))
            {
                try
                {
                    var serializer = new DataContractJsonSerializer(typeof (TranslationRootElement));
                    var rootElement = (TranslationRootElement) serializer.ReadObject(memoryStream);
                    return rootElement;
                }
                catch (InvalidDataContractException e)
                {
                    throw new TranslateException(TranslateError.JsonParserError, e.Message);
                }
                catch (SerializationException e)
                {
                    throw new TranslateException(TranslateError.JsonParserError, e.Message);
                }
            }
        }

        public TranslationRootElement Parse(byte[] jsonData)
        {
            using (var memoryStream = new MemoryStream(jsonData))
            {
                try
                {
                    var serializer = new DataContractJsonSerializer(typeof(TranslationRootElement));
                    var rootElement = (TranslationRootElement)serializer.ReadObject(memoryStream);
                    return rootElement;
                }
                catch (InvalidDataContractException e)
                {
                    throw new TranslateException(TranslateError.JsonParserError, e.Message);
                }
                catch (SerializationException e)
                {
                    throw new TranslateException(TranslateError.JsonParserError, e.Message);
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

using BootGenius.GoogleTranslator.Enums;
using BootGenius.GoogleTranslator.Helpers;

namespace BootGenius.GoogleTranslator.Components
{
    internal class GoogleTranslateRequestBuilder
    {
        public string GoogleTranslateUrl
        {
            get { return "translate.google.ru"; }
        }

        public Uri GetTranslationUri(TranslateLanguage fromLanguage,
                                     TranslateLanguage toLanguage,
                                     IEnumerable<string> stringsToTranslate)
        {
            var parameters = new List<string>();
            parameters.Add(String.Format("client={0}", "p")); // response in JSON format
            parameters.Add(String.Format("text={0}", GetEncodedText(stringsToTranslate))); // text to translate
            parameters.Add(String.Format("hl={0}", GoogleTranslatorHelper.ConvertLanguageToRequestString(toLanguage))); // interface language
            parameters.Add(String.Format("sl={0}", GoogleTranslatorHelper.ConvertLanguageToRequestString(fromLanguage))); // source language
            parameters.Add(String.Format("tl={0}", GoogleTranslatorHelper.ConvertLanguageToRequestString(toLanguage))); // translated language
            parameters.Add(String.Format("ie={0}", "UTF-8")); // input encoding
            parameters.Add(String.Format("oe={0}", "UTF-8")); // output encoding
            parameters.Add(String.Format("multires={0}", "1")); // unknown
            //parameters.Add(String.Format("prev={0}", "btn"));                                                                 // unknown (temporary commented)
            parameters.Add(String.Format("ssel={0}", "0")); // unknown (temporary commented)
            parameters.Add(String.Format("tsel={0}", "0")); // unknown (temporary commented)
            parameters.Add(String.Format("sc={0}", "1")); // unknown (temporary commented)

            return BuildUri(GoogleTranslateUrl,
                            @"translate_a/t",
                            parameters);
        }

        public Uri GetPronunciationUri(TranslateLanguage language,
                                       string text)
        {
            var parameters = new List<string>();
            parameters.Add(String.Format("ie={0}", "UTF-8"));                                                           // input encoding
            parameters.Add(String.Format("q={0}", GetEncodedText(new[]{text})));                                        // text to translate
            parameters.Add(String.Format("tl={0}", GoogleTranslatorHelper.ConvertLanguageToRequestString(language)));   // language to pronounce
            parameters.Add(String.Format("total={0}", 1));                                                              // unknown total parameter
            parameters.Add(String.Format("idx={0}", 0));                                                                // unknown idx parameter
            parameters.Add(String.Format("textlen={0}", text.Length));                                                  // length of the text

            return BuildUri(GoogleTranslateUrl,
                            @"translate_tts",
                            parameters);            
        }

        private static Uri BuildUri(string hostName,
                                    string path,
                                    IEnumerable<string> parameters)
        {
            var uriBuilder = new UriBuilder
            {
                Host = hostName,
                Path = path,
                Query = String.Join("&", parameters)
            };
            return uriBuilder.Uri;
        }

        private static string GetEncodedText(IEnumerable<string> stringsToTranslate)
        {
            return HttpUtility.UrlEncodeUnicode(String.Join(Environment.NewLine, stringsToTranslate));
        }
    }
}

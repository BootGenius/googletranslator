﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

using BootGenius.GoogleTranslator.Enums;
using BootGenius.GoogleTranslator.EventArgs;
using BootGenius.GoogleTranslator.Exceptions;
using BootGenius.GoogleTranslator.Model;

namespace BootGenius.GoogleTranslator.Components
{
    public class GoogleTranslator
    {
        #region Fields

        readonly GoogleTranslateRequestBuilder _requestBuilder = new GoogleTranslateRequestBuilder();

        #endregion

        #region Events

        public event EventHandler<GetTextPronunciationCompletedEventArgs> GetTextPronunciationCompleted;

        public event EventHandler<TranslateTextCompletedEventArgs> TranslateTextCompleted;

        public event EventHandler<TranslateSingleWordCompletedEventArgs> TranslateSingleWordCompleted;

        #endregion

        #region Event Invokators

        protected virtual void OnGetTextPronunciationCompleted(GetTextPronunciationCompletedEventArgs e)
        {
            var handler = GetTextPronunciationCompleted;
            if (handler != null) 
                handler(this, e);
        }

        protected virtual void OnTranslateTextCompleted(TranslateTextCompletedEventArgs e)
        {
            var handler = TranslateTextCompleted;
            if (handler != null) 
                handler(this, e);
        }

        protected virtual void OnTranslateSingleWordCompleted(TranslateSingleWordCompletedEventArgs e)
        {
            var handler = TranslateSingleWordCompleted;
            if (handler != null) handler(this, e);
        }

        #endregion

        #region Public Methods

        public byte[] GetTextPronunciation(string sourceString, TranslateLanguage language)
        {
            var webClient = GetWebClient();
            var requestString = _requestBuilder.GetPronunciationUri(language, sourceString);

            try
            {
                var data = webClient.DownloadData(requestString);
                return data;
            }
            catch (WebException e)
            {
                throw new TranslateException(TranslateError.HttpRequestError, e.Message);
            }
        }

        public void GetTextPronunciationAsync(string sourceString, TranslateLanguage language)
        {
            var webClient = GetWebClient();
            webClient.DownloadDataCompleted += WebClientOnGetTextPronunciationDownloadDataCompleted;

            var requestString = _requestBuilder.GetPronunciationUri(language, sourceString);
            webClient.DownloadDataAsync(requestString);
        }

        public string TranslateText(string sourceString, TranslateLanguage fromLanguage, TranslateLanguage toLanguage)
        {
            var webClient = GetWebClient();
            var requestString = _requestBuilder.GetTranslationUri(fromLanguage, toLanguage, new List<string> {sourceString});

            try
            {
                var data = webClient.DownloadData(requestString);
                var rootElement = ParseResponseData(data);
                if (rootElement != null && rootElement.Sentences != null && rootElement.Sentences.Any())
                    return rootElement.Sentences.First().TranslatedText;

                return string.Empty;
            }
            catch (WebException e)
            {
                throw new TranslateException(TranslateError.HttpRequestError, e.Message);
            }
        }

        public void TranslateTextAsync(string sourceString, TranslateLanguage fromLanguage, TranslateLanguage toLanguage)
        {
            var webClient = GetWebClient();
            webClient.DownloadDataCompleted += WebClientOnTranslateTextDownloadDataCompleted;

            var requestString = _requestBuilder.GetTranslationUri(fromLanguage, toLanguage, new List<string> { sourceString });
            webClient.DownloadDataAsync(requestString);
        }

        public TranslationRootElement TranslateSingleWord(string sourceString, TranslateLanguage fromLanguage, TranslateLanguage toLanguage)
        {
            var webClient = GetWebClient();
            var requestString = _requestBuilder.GetTranslationUri(fromLanguage, toLanguage, new List<string> { sourceString });

            try
            {
                var data = webClient.DownloadData(requestString);
                var rootElement = ParseResponseData(data);
                return rootElement;
            }
            catch (WebException e)
            {
                throw new TranslateException(TranslateError.HttpRequestError,e.Message);
            }
        }

        public void TranslateSingleWordAsync(string sourceWord, TranslateLanguage fromLanguage, TranslateLanguage toLanguage)
        {
            var webClient = GetWebClient();
            webClient.DownloadDataCompleted += WebClientOnTranslateSingleWordDownloadDataCompleted;

            var requestString = _requestBuilder.GetTranslationUri(fromLanguage, toLanguage, new List<string> { sourceWord });
            webClient.DownloadDataAsync(requestString);
        }

        #endregion

        #region EventHandlers

        private void WebClientOnGetTextPronunciationDownloadDataCompleted(object sender, DownloadDataCompletedEventArgs eventArgs)
        {
            var webClient = sender as WebClient;
            if (webClient == null)
                return;

            webClient.DownloadDataCompleted -= WebClientOnGetTextPronunciationDownloadDataCompleted;

            if (eventArgs.Error != null)
            {
                OnGetTextPronunciationCompleted(new GetTextPronunciationCompletedEventArgs(new TranslateException(TranslateError.HttpRequestError, eventArgs.Error.Message)));
                return;
            }

            try
            {
                OnGetTextPronunciationCompleted(new GetTextPronunciationCompletedEventArgs(eventArgs.Result));
            }
            catch (TranslateException e)
            {
                OnGetTextPronunciationCompleted(new GetTextPronunciationCompletedEventArgs(e));
            }
            catch (Exception e)
            {
                OnGetTextPronunciationCompleted(new GetTextPronunciationCompletedEventArgs(new TranslateException(TranslateError.UnknownError, e.Message)));
            }
        }

        private void WebClientOnTranslateSingleWordDownloadDataCompleted(object sender, DownloadDataCompletedEventArgs eventArgs)
        {
            var webClient = sender as WebClient;
            if (webClient == null)
                return;

            webClient.DownloadDataCompleted -= WebClientOnTranslateSingleWordDownloadDataCompleted;

            if (eventArgs.Error != null)
            {
                OnTranslateSingleWordCompleted(new TranslateSingleWordCompletedEventArgs(new TranslateException(TranslateError.HttpRequestError, eventArgs.Error.Message)));
                return;
            }

            try
            {
                var rootElement = ParseResponseData(eventArgs.Result);
                OnTranslateSingleWordCompleted(new TranslateSingleWordCompletedEventArgs(rootElement));
            }
            catch (TranslateException e)
            {
                OnTranslateSingleWordCompleted(new TranslateSingleWordCompletedEventArgs(e));
            }
            catch (Exception e)
            {
                OnTranslateSingleWordCompleted(new TranslateSingleWordCompletedEventArgs(new TranslateException(TranslateError.UnknownError, e.Message)));
            }
        }

        private void WebClientOnTranslateTextDownloadDataCompleted(object sender, DownloadDataCompletedEventArgs eventArgs)
        {
            var webClient = sender as WebClient;
            if (webClient == null)
                return;

            webClient.DownloadDataCompleted -= WebClientOnTranslateTextDownloadDataCompleted;

            if (eventArgs.Error != null)
            {
                OnTranslateTextCompleted(new TranslateTextCompletedEventArgs(new TranslateException(TranslateError.HttpRequestError, eventArgs.Error.Message)));
                return;
            }

            try
            {
                var rootElement = ParseResponseData(eventArgs.Result);
                string translatedText = String.Empty;
                if (rootElement != null && rootElement.Sentences != null && rootElement.Sentences.Any())
                    translatedText = rootElement.Sentences.First().TranslatedText;

                OnTranslateTextCompleted(new TranslateTextCompletedEventArgs(translatedText));
            }
            catch (TranslateException e)
            {
                OnTranslateTextCompleted(new TranslateTextCompletedEventArgs(e));
            }
            catch (Exception e)
            {
                OnTranslateTextCompleted(new TranslateTextCompletedEventArgs(new TranslateException(TranslateError.UnknownError, e.Message)));
            }
        }

        #endregion

        #region Private Methods

        private WebClient GetWebClient()
        {
            var webClient = new WebClient
                {
                    Proxy = null
                };
            return webClient;
        }

        private TranslationRootElement ParseResponseData(byte[] data)
        {
            var parser = new GoogleTranslateParser();
            var rootElement = parser.Parse(data);
            return rootElement;
        }
    }

    #endregion

}

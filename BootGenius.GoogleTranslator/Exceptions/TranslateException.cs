﻿using System;

using BootGenius.GoogleTranslator.Enums;

namespace BootGenius.GoogleTranslator.Exceptions
{
    public class TranslateException : Exception
    {
        public TranslateError ErrorType { get; private set; }

        public TranslateException(TranslateError errorType, string errorMessage) : base(errorMessage)
        {
            ErrorType = errorType;
        }

        public TranslateException(TranslateError errorType)
        {
            ErrorType = errorType;
        }
    }
}

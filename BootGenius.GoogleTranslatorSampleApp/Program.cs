﻿using BootGenius.GoogleTranslator.Enums;
using BootGenius.GoogleTranslator.EventArgs;

namespace BootGenius.GoogleTranslatorSampleApp
{
    class Program
    {
        #region .ctor

        private static void Main(string[] args)
        {
            var googleTranslator = new GoogleTranslator.Components.GoogleTranslator();

            //1. Translate text sync example
            var translation = googleTranslator.TranslateText("I want to translate this text", TranslateLanguage.English, TranslateLanguage.Russian);

            //2. Translate text async example
            googleTranslator.TranslateTextCompleted += GoogleTranslatorOnTranslateTextCompleted;
            googleTranslator.TranslateTextAsync("I want to translate this text", TranslateLanguage.English, TranslateLanguage.Russian);

            //3. Translate text sync example
            var singleWordTranslation = googleTranslator.TranslateSingleWord("Hello", TranslateLanguage.English, TranslateLanguage.Russian);

            //4. Translate text async example
            googleTranslator.TranslateSingleWordCompleted += GoogleTranslatorOnTranslateSingleWordCompleted;
            googleTranslator.TranslateSingleWordAsync("Hello", TranslateLanguage.English, TranslateLanguage.Russian);


            //5. Get text pronunciation sync example
            byte[] translationData = googleTranslator.GetTextPronunciation("Hello", TranslateLanguage.English);

            //6. Get text pronunciation async example
            googleTranslator.GetTextPronunciationCompleted += GoogleTranslatorOnGetTextPronunciationCompleted;
            googleTranslator.GetTextPronunciationAsync("Hello", TranslateLanguage.English);
        }

        #endregion


        #region EventHandlers

        private static void GoogleTranslatorOnGetTextPronunciationCompleted(object sender, GetTextPronunciationCompletedEventArgs eventArgs)
        {
            if (eventArgs.Error == null)
            {
                byte[] translationData = eventArgs.Result;
            }
        }

        private static void GoogleTranslatorOnTranslateTextCompleted(object sender, TranslateTextCompletedEventArgs eventArgs)
        {
            if (eventArgs.Error == null)
            {
                string translation = eventArgs.Result;
            }
        }

        private static void GoogleTranslatorOnTranslateSingleWordCompleted(object sender, TranslateSingleWordCompletedEventArgs eventArgs)
        {
            if (eventArgs.Error == null)
            {
                var result = eventArgs.Result;
            }
        }

        #endregion
    }
}
